# Setux Targets

[Setux] Targets

[PyPI] - [Repo] - [Doc]


## [Target] implementations
 * Local
 * SSH

## Installation

    $ pip install setux_targets


[PyPI]: https://pypi.org/project/setux_targets
[Repo]: https://gitlab.com/dugres/setux_targets
[Doc]: https://setux-targets.readthedocs.io/en/latest
[Setux]: https://setux.readthedocs.io/en/latest

[Target]: https://setux-core.readthedocs.io/en/latest/target
