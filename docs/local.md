# Local
`setux.targets.local`

[Setux] Local Target

[PyPI] - [Repo] - [Doc]


[Target] implementation

`setux.core.target.Target`



[PyPI]: https://pypi.org/project/setux_targets
[Repo]: https://gitlab.com/dugres/setux_targets
[Doc]: https://setux-targets.readthedocs.io/en/latest/group
[Setux]: https://setux.readthedocs.io/en/latest

[Target]: https://setux-core.readthedocs.io/en/latest/target
