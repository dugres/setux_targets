# Setux Targets

[Setux] Targets

[PyPI] - [Repo] - [Doc]


[PyPI]: https://pypi.org/project/setux_targets
[Repo]: https://gitlab.com/dugres/setux_targets
[Doc]: https://setux-targets.readthedocs.io/en/latest
[Setux]: https://setux.readthedocs.io/en/latest
